'use strict';

const express = require('express');
const bodyParser = require('body-parser');

const app = express();

// * LOAD ROUTES

// * MIDDLEWARES
app.use( bodyParser.urlencoded({ extended: false }) );
app.use( bodyParser.json() );

// * CORS

// * ROUTES

app.get('/', (req, res) => {
    res.status(200).send({
        status: 'ok',
        message: 'Hola mundo desde el servidor de NodeJS'
    });
});

app.get('/pruebas', (req, res) => {

    res.status(200).send({
        status: 'ok',
        message: 'Acción de pruebas en el servidor de NodeJS'
    });
});

// * Export
module.exports = app;