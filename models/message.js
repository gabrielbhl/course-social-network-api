'use strict'

const mongoose = require('mongoose')

const Schema = mongoose.Schema

const MessageSchema = Schema({
    text: String,
    created: String,
    emmiter: { type: Schema.ObjectId, ref: 'User'},
    reciever: { type: Schema.ObjectId, ref: 'User'}
})

module.exports = mongoose.model('Message', MessageSchema)
